jQuery(document).ready(function() {
  if (typeof CodeMirror != 'undefined' && jQuery('#mr-scss-themes-scss').length) {
    var scssCodeMirror = CodeMirror.fromTextArea(document.getElementById("mr-scss-theme-scss"), {
        lineNumbers: true,
        matchBrackets: true,
        mode: "text/x-scss",
        firstLineNumber: 1
      }
    );
    scssCodeMirror.refresh();
  }
});