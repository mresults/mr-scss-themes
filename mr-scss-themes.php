<?php

/**
 * Plugin Name: SCSS Themes
 * Plugin URI: https://bitbucket.org/mresults/
 * Description: Manage SCSS Themes for incorporation into content
 * Version: 0.0.3
 * Author: Marketing Results
 * Author URI: https://www.marketingresults.com.au
 * License: GPLv3
 * Bitbucket Plugin URI: mresults/mr-scss-themes
 * Bitbucket Branch:    master
 */

require_once('inc/register-content-types.php');
require_once('inc/register-fields.php');

// Enqueue SCSS themes
add_action('get_header', 'mr_scss_theme_get_post_themes', 1, 2);

// Perform some setup for the admin area
if (is_admin()) {
	add_action('admin_menu', 'mr_scss_theme_admin_menu');
  define( 'ACF_LITE', true );
  mr_scss_theme_register_theme_to_code_relation();
  mr_scss_theme_register_post_to_theme_relation();
}

// Add the admin menu items
function mr_scss_theme_admin_menu() {
	add_menu_page('SCSS Themes', 'SCSS Themes', 'manage_options', 'edit.php?post_type=scss_theme', NULL, plugin_dir_url(__FILE__) . 'images/sass-icon.png');
	add_submenu_page('edit.php?post_type=scss_theme', 'SCSS Themes', 'Add New Theme', 'manage_options', 'post-new.php?post_type=scss_theme');
	add_submenu_page('edit.php?post_type=scss_theme', 'SCSS Themes', 'Manage Code Blocks', 'manage_options', 'edit.php?post_type=scss_codeblock');
	add_submenu_page('edit.php?post_type=scss_theme', 'SCSS Themes', 'Add New Code Block', 'manage_options', 'post-new.php?post_type=scss_codeblock');
}

// Add any SCSS themes for a particular post to the CSS render queue
function mr_scss_theme_get_post_themes() {
    
    global $wp_query;
    
    if (!$wp_query->is_singular) {
        return;
    }
    
    $meta = get_post_meta($wp_query->post->ID);

   if (isset($meta['scss_themes'])) {
       add_filter('wp_scss_scss_raws', 'mr_scss_theme_scssraw');
       add_filter('body_class', 'mr_scss_theme_apply_body_class');
   }
}

// Get all SCSS theme code related to a post and add it to the WP SCSS compilation queue
function mr_scss_theme_scssraw($scss_raw) {
    
    global $post;
    
    $meta = get_post_meta($post->ID);
    $scss_themes = $meta['scss_themes'];
    unset($meta);
    
    // There should only be one SCSS theme field but who knows
    foreach ($scss_themes as $scss_theme) {
      
        // Get the attached themes for this field
        $scss_theme_entries = unserialize($scss_theme);
        
        // Iterate through them
        foreach ($scss_theme_entries as $scss_theme_entry) {
          
            // Get the theme
            $theme = get_post($scss_theme_entry);
            
            // Initialise a timestamp
            $timestamp = 0;
            
            $scss_raw_part = array();
            $meta = get_post_meta($theme->ID);
            
            // If the theme has attached code blocks
            if (isset($meta['code_blocks'])) {
              
                // Initialise an array of code chunks for this theme
                $scss_raw_part[$theme->ID] = array();
                
                // Set the raw SCSS item's timestamp to the theme's last modification date, or the current timestamp,
                // whichever is greater
                $timestamp = (strtotime($theme->post_modified) > $timestamp) ? strtotime($theme->post_modified) : $timestamp;
                
                $scss_blocks = $meta['code_blocks'];
                unset($meta);
                
                // There should only be the one code block field but who knows
                foreach ($scss_blocks as $scss_block) {
                  
                    // Get the code blocks attached to this field
                    $scss_block_entries = unserialize($scss_block);
                  
                    // Iterate them
                    foreach ($scss_block_entries as $scss_block_entry) {
                      
                        // Get the code block
                        $block = get_post($scss_block_entry);
                        $meta = get_post_meta($block->ID);
                        
                        // Set the theme's timestamp to this code block's last modified date, or the current timestamp,
                        // whichever is greater
                        $timestamp = (strtotime($theme->post_modified) > $timestamp) ? strtotime($block->post_modified) : $timestamp;
                        
                        // Assuming the code block has SCSS ...
                        if (isset($meta['mr-scss-theme-scss'])) {
                          
                            // Push the SCSS to the array of SCSS blocks for this theme
                            $scss_raw_part[$theme->ID][] = implode("\n", $meta['mr-scss-theme-scss']);
                        }
                    }
                }
            }
            
            // Generate a raw SCSS block from this theme for WP-SCSS 
            $scss_raw[] = array(
            
              // The handle is the raw SCSS block's identifier, and is used to generate a CSS filename
              'handle' => "mr-scss-theme-{$theme->ID}",
              
              // The timestamp is used by WP-SCSS to determine whether it needs to regenerate the CSS or not.
              // It should be set to the latest modification date for the theme or any of its code blocks
              'timestamp' => $timestamp,
              
              // All the SCSS code blocks get imploded and wrapped inside a unique class using this theme's ID
              'raw' => ".mr-scss-theme-{$theme->ID} {\n" . implode("\n", $scss_raw_part[$theme->ID]) . "\n}",
            );
        }
    }

    return $scss_raw;
}

// Apply a class to the theme's body element so that the SCSS Theme CSS is applied to the post
function mr_scss_theme_apply_body_class($classes) {
    global $post;
    
    $meta = get_post_meta($post->ID);
    $scss_themes = $meta['scss_themes'];
    unset($meta);
    
    // There should only be one SCSS Themes field, but who knows ...
    foreach ($scss_themes as $scss_theme) {
      
        // Get the themes attached to this field
        $scss_theme_entries = unserialize($scss_theme);
        
        // Iterate through them
        foreach ($scss_theme_entries as $scss_theme_entry) {
          
          // Append a class using this theme's ID to the body classes array
          $classes[] = "mr-scss-theme-{$scss_theme_entry}";
        }
    }
    
    return $classes;
}
    
    
    

?>