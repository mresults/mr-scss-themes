<?php

include_once('../lib/advanced-custom-fields/acf.php');

// Register the Meta Box for SCSS code blocks on post edit / add
add_action( 'load-post.php', 'mr_scss_theme_meta_boxes_setup' );
add_action( 'load-post-new.php', 'mr_scss_theme_meta_boxes_setup' );

// Register the Meta Box for SCSS code blocks
function mr_scss_theme_meta_boxes_setup() {

  /* Add meta box on the 'add_meta_boxes' hook. */
  add_action( 'add_meta_boxes', 'mr_scss_theme_add_post_meta_boxes' );
  
  /* Save post meta on the 'save_post' hook. */
  add_action( 'save_post', 'mr_scss_theme_save_scss_code_meta', 10, 2 );
  
}

// Create the code block Meta Box
function mr_scss_theme_add_post_meta_boxes() {

  add_meta_box(
    'mr-scss-theme-scss-wrapper',      // Unique ID
    esc_html__( 'SCSS Code', 'example' ),    // Title
    'mr_scss_theme_scss_meta_box',   // Callback function
    'scss_codeblock',         // Admin page (or post type)
    'normal',         // Context
    'default'         // Priority
  );

  // Add a bunch of Codemirror dependencies for fancy code rendering
  wp_enqueue_style('mr-scss-theme-scss-code-field-codemirror-css', plugin_dir_url(__FILE__) . 'lib/codemirror/lib/codemirror.css');
  wp_enqueue_style('mr-scss-theme-scss-code-field-codemirror-gutter', plugin_dir_url(__FILE__) . 'css/codemirror.css');
  
  wp_register_script('mr-scss-theme-scss-code-field-codemirror-js', plugin_dir_url(__FILE__) . 'lib/codemirror/lib/codemirror.js', array('jquery-core'));
  wp_register_script('mr-scss-theme-scss-code-field-codemirror-js-scss-mode', plugin_dir_url(__FILE__) . 'lib/codemirror/mode/css/css.js', array('jquery-core'));
  wp_register_script('mr-scss-theme-scss-field', plugin_dir_url(__FILE__) . 'js/scss-field.js', array('jquery-core'));
  
  wp_enqueue_script('mr-scss-theme-scss-code-field-codemirror-js');
  wp_enqueue_script('mr-scss-theme-scss-code-field-codemirror-js-scss-mode');
  wp_enqueue_script('mr-scss-theme-scss-field');
  
}

// Display the SCSS Code Block metabox 
function mr_scss_theme_scss_meta_box( $object, $box ) { ?>

  <?php wp_nonce_field( basename( __FILE__ ), 'mr_scss_theme_class_nonce' ); ?>

  <p>
    <textarea class="widefat" name="mr-scss-theme-scss" id="mr-scss-theme-scss" /><?php echo esc_attr( get_post_meta( $object->ID, 'mr-scss-theme-scss', true ) ); ?></textarea>
  </p>
<?php }

/* Save the meta box's post metadata. */
function mr_scss_theme_save_scss_code_meta( $post_id, $post ) {

  /* Verify the nonce before proceeding. */
  if ( !isset( $_POST['mr_scss_theme_class_nonce'] ) || !wp_verify_nonce( $_POST['mr_scss_theme_class_nonce'], basename( __FILE__ ) ) )
    return $post_id;

  /* Get the post type object. */
  $post_type = get_post_type_object( $post->post_type );

  /* Check if the current user has permission to edit the post. */
  if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
    return $post_id;

  /* Get the posted data and sanitize it for use as an HTML class. */
  $new_meta_value = ( isset( $_POST['mr-scss-theme-scss'] ) ? $_POST['mr-scss-theme-scss'] : '' );

  /* Get the meta key. */
  $meta_key = 'mr-scss-theme-scss';

  /* Get the meta value of the custom field key. */
  $meta_value = get_post_meta( $post_id, $meta_key, true );

  /* If a new meta value was added and there was no previous value, add it. */
  if ( $new_meta_value && '' == $meta_value )
    add_post_meta( $post_id, $meta_key, $new_meta_value, true );

  /* If the new meta value does not match the old value, update it. */
  elseif ( $new_meta_value && $new_meta_value != $meta_value )
    update_post_meta( $post_id, $meta_key, $new_meta_value );

  /* If there is no new meta value but an old value exists, delete it. */
  elseif ( '' == $new_meta_value && $meta_value )
    delete_post_meta( $post_id, $meta_key, $meta_value );
}

// Register Theme to Code Block relationship field
function mr_scss_theme_register_theme_to_code_relation() {
    if(function_exists("register_field_group"))
    {
        register_field_group(array (
            'id' => 'acf_code-blocks',
            'title' => 'Code Blocks',
            'fields' => array (
                array (
                    'key' => 'field_550d2b91d7954',
                    'label' => 'Code Blocks',
                    'name' => 'code_blocks',
                    'type' => 'relationship',
                    'instructions' => 'SCSS Code Blocks to use in this theme',
                    'return_format' => 'object',
                    'post_type' => array (
                        0 => 'scss_codeblock',
                    ),
                    'taxonomy' => array (
                        0 => 'all',
                    ),
                    'filters' => array (
                        0 => 'search',
                    ),
                    'result_elements' => array (
                        0 => 'post_title',
                    ),
                    'max' => '',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'scss_theme',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'normal',
                'layout' => 'no_box',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));
    }
}

// Register Post to Theme relationship field
function mr_scss_theme_register_post_to_theme_relation() {
    if(function_exists("register_field_group"))
    {
        register_field_group(array (
            'id' => 'acf_scss-themes',
            'title' => 'SCSS Themes',
            'fields' => array (
                array (
                    'key' => 'field_550d638e5f62d',
                    'label' => 'SCSS Themes',
                    'name' => 'scss_themes',
                    'type' => 'relationship',
                    'return_format' => 'object',
                    'post_type' => array (
                        0 => 'scss_theme',
                    ),
                    'taxonomy' => array (
                        0 => 'all',
                    ),
                    'filters' => array (
                        0 => 'search',
                    ),
                    'result_elements' => array (
                        0 => 'post_title',
                    ),
                    'max' => '',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'post',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page',
                        'order_no' => 0,
                        'group_no' => 1,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'normal',
                'layout' => 'no_box',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));
    }

}

?>