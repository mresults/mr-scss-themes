<?php

// Register the SCSS Theme Content Type
function mr_scss_theme_register_scss_theme() {

	$labels = array(
		'name'                => _x( 'SCSS Themes', 'Post Type General Name', 'scss_theme' ),
		'singular_name'       => _x( 'SCSS Theme', 'Post Type Singular Name', 'scss_theme' ),
		'menu_name'           => __( 'SCSS Theme', 'scss_theme' ),
		'parent_item_colon'   => __( 'Parent Theme:', 'scss_theme' ),
		'all_items'           => __( 'All Themes', 'scss_theme' ),
		'view_item'           => __( 'View Theme', 'scss_theme' ),
		'add_new_item'        => __( 'Add New Theme', 'scss_theme' ),
		'add_new'             => __( 'Add New', 'scss_theme' ),
		'edit_item'           => __( 'Edit Theme', 'scss_theme' ),
		'update_item'         => __( 'Update Theme', 'scss_theme' ),
		'search_items'        => __( 'Search Theme', 'scss_theme' ),
		'not_found'           => __( 'Not found', 'scss_theme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'scss_theme' ),
	);
	$args = array(
		'label'               => __( 'scss_theme', 'scss_theme' ),
		'description'         => __( 'Themes containing sets of SCSS, to attach to content with special design reuirements', 'scss_theme' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'revisions', 'author'),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 60,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'rewrite'             => false,
		'capability_type'     => 'page',
	);
	register_post_type( 'scss_theme', $args );

}

// Register the SCSS Code Block Content Type
function mr_scss_theme_register_scss_codeblock() {

	$labels = array(
		'name'                => _x( 'SCSS Code Block', 'Post Type General Name', 'scss_codeblock' ),
		'singular_name'       => _x( 'SCSS Code Block', 'Post Type Singular Name', 'scss_codeblock' ),
		'menu_name'           => __( 'SCSS Code Block', 'scss_codeblock' ),
		'parent_item_colon'   => __( 'Parent Code Block:', 'scss_codeblock' ),
		'all_items'           => __( 'All Code Blocks', 'scss_codeblock' ),
		'view_item'           => __( 'View Code Block', 'scss_codeblock' ),
		'add_new_item'        => __( 'Add New Code Block', 'scss_codeblock' ),
		'add_new'             => __( 'Add New', 'scss_codeblock' ),
		'edit_item'           => __( 'Edit Code Block', 'scss_codeblock' ),
		'update_item'         => __( 'Update Code Block', 'scss_codeblock' ),
		'search_items'        => __( 'Search Code Block', 'scss_codeblock' ),
		'not_found'           => __( 'Not found', 'scss_codeblock' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'scss_codeblock' ),
	);
	$args = array(
		'label'               => __( 'scss_codeblock', 'scss_codeblock' ),
		'description'         => __( 'SCSS Code Blocks attached to SCSS Themes', 'scss_codeblock' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'revisions', 'author'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 60,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'rewrite'             => false,
		'capability_type'     => 'page',
	);
	register_post_type( 'scss_codeblock', $args );

}

// Register SCSS Theme Content Type pn initialisation
add_action( 'init', 'mr_scss_theme_register_scss_theme', 0 );

// Register SCSS Code Block Content Type pn initialisation
add_action( 'init', 'mr_scss_theme_register_scss_codeblock', 0 );

?>